<?php

namespace frontend\controllers;

use frontend\models\Brand;
use frontend\models\Distance;
use frontend\models\Orders;
use frontend\models\Years;
use frontend\models\LifeInsurance;
use yii\base\Object;
use \yii\web\Controller;
use Yii;

class LifeinsuranceController extends Controller
{

    public function actionIndex()
    {
        $result = false;
        $resultBrand = false;

        $distance = Distance::find()->asArray()->all();//Достаем все из БД - пробег
        $years = Years::find()->asArray()->all();//Достаем все из БД - год выпуска

        $order = new Orders();
        $model = new Lifeinsurance();

        if ($order->load(Yii::$app->request->post())) {//&& $order->save()
            echo '<pre>';
            echo '$order :' . $order['brand'] . '<br>';
            //print_r($order);
            echo '<hr>';
            echo '</hr>';
            $countBrand = self::countBrand();
            //$countYears = self::countYears();
            print_r($countBrand);
            //print_r($countYears);
            echo '<hr>';
            if (!empty($countBrand && $order['distance'] && $order['years'])) {
                $result = Orders::getResult($countBrand, $order['distance'], $order['years']);
            }
            echo '</pre>';
            echo '<hr>';

            //Yii::$app->session->setFlash('danger', 'Данные загружены не все');
            //return $this->refresh();
        }

        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->setFlash('success', 'Данные загружены в страховку жизни');
            //return $this->refresh();
        }

        return $this->render('index',[
            'model' => $model,
            'distance' => $distance,
            'years' => $years,
            'order' => $order,
            'result' => $result,
            'resultBrand' => $resultBrand,
        ]);
    }


    /**
     * @return Object |\yii\db\ActiveRecord
     */
    public static function getBrand()
    {
        $order = new Orders();
        if ($order->load(Yii::$app->request->post())) {
            $findBrand = Yii::$app->request->post();
            $brand = Brand::find()
                ->where(['brand' => $findBrand])
                ->one();
            if (isset($brand->attributes['brand'])) {
                if ($order->attributes['brand'] == $brand->attributes['brand']) {
                    if (is_object($brand)) {
                        return $brand;
                    }
                }
            }
        }
    }

//    /**
//     * @return Object |\yii\db\ActiveRecord
//     */
//    public static function getYears()
//    {
//        $order = new Orders();
//        if ($order->load(Yii::$app->request->post())) {
//            $findYears = Yii::$app->request->post();
//            print_r($findYears);
//            $years = Years::find()
//                ->where(['brand' => $findYears])
//                ->one();
//            print_r($years);
//            die;
//
//            if (isset($brand->attributes['brand'])) {
//                if ($order->attributes['brand'] == $brand->attributes['brand']) {
//                    if (is_object($brand)) {
//                        return $brand;
//                    }
//                }
//            }
//        }
//    }


    public function countBrand()
    {
        $order = new Orders();
        if ($order->load(Yii::$app->request->post())) {
            $brand = self::getBrand();
            if (isset($brand) && is_object($brand)){
                echo 'Obj';
                $resultBrand = $brand->attributes['brand_count'];
                return $resultBrand;
            } else {
                return Yii::$app->session->setFlash('danger', 'Такой марки машины не существует в нашей БД, обратитесь к администратору');
            }
        }
    }

//    public function countYears()
//    {
//        $order = new Orders();
//        if ($order->load(Yii::$app->request->post())) {
//            $years = self::getYears();
//            if (isset($years) && is_object($years)){
//                echo 'Obj';
//                $resultBrand = $years->attributes['brand_count'];
//                return $resultBrand;
//            } else {
//                return Yii::$app->session->setFlash('danger', 'Такой даты выпуска машины не существует в нашей БД, обратитесь к администратору');
//            }
//        }
//    }
}
