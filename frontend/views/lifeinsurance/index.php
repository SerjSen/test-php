<?php
/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<h1>Введите ваши данные</h1>

<h5>* - поля обязательны для заполнения</h5>
<hr>
<p><h4>Введите данные машины: </h4></p>
<div class="container">
    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($order,'brand') ?>
    <?php echo $form->field($order,'distance') ?>
    <?php echo $form->field($order,'years') ?>
    <?php echo Html::submitButton('Save', [
        'class' => 'btn btn-danger',
    ]); ?>
    <?php ActiveForm::end();?>
</div>
<p><h1>
    <?php
    if(!empty($result)){
        echo 'Цена: ' . $result;
    } else {
        echo 'Пока нету результатов';
    }
    ?>
</h1></p>
<hr>
<p><h4>Введите ваши данные:</h4></p>
<div class="container">
    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model,'first_name') ?>
    <?php echo $form->field($model,'first_name') ?>
    <?php echo $form->field($model,'patronymic') ?>
    <?php echo $form->field($model,'last_name') ?>
    <?php echo $form->field($model,'telephone')->textInput(array('placeholder' => 'В нужном формате: (xxx) xxx xx xx')); ?>
    <?php echo $form->field($model,'country') ?>
    <?php echo $form->field($model,'city') ?>
    <?php echo $form->field($model,'region') ?>
    <?php echo $form->field($model,'date_of_birth') ?>
    <?php echo $form->field($model,'work') ?>
    <?php echo $form->field($model,'bad_habits') ?>
    <?php echo $form->field($model,'comment') ?>
    <?php echo Html::submitButton('Save', [
        'class' => 'btn btn-primary',
    ]); ?>
    <?php ActiveForm::end();?>
</div>
