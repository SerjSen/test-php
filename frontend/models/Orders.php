<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $brand
 * @property integer $distance
 * @property integer $years
 * @property integer $result_count
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand','distance','years'], 'required'],
            [['user_id', 'distance', 'years', 'result_count'], 'integer'],
            [['brand'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'brand' => 'Brand',
            'distance' => 'Distance',
            'years' => 'Years',
            'result_count' => 'Result Count',
        ];
    }

    public function getLifeinsurance()
    {
        return $this->hasOne(LifeInsurance::className(),['id' => 'user_id']);
    }

    /**
     * Функция в какой подсчет алгоритма
     * @param $brand
     * @param $year
     * @param $distance
     * @return int|string
     */
    public static function getResult($brand,$year,$distance){
        if (!empty($brand && $year && $distance)) {
            print_r($brand);
            if(is_numeric($brand) && is_numeric($year) && is_numeric($distance)) {
                $result = $brand + $year + $distance;
                return $result;
            } else {
                echo ' ( не числа ) ';
            }
        }
    }
}
