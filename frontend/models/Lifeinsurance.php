<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "life_insurance".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $patronymic
 * @property string $last_name
 * @property string $telephone
 * @property string $country
 * @property string $city
 * @property string $region
 * @property string $date_of_birth
 * @property string $work
 * @property string $comment
 * @property string $bad_habits
 */
class LifeInsurance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'life_insurance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['first_name', 'patronymic', 'last_name', 'telephone', 'country', 'city', 'region', 'date_of_birth', 'work', 'comment', 'bad_habits'], 'required'],
            [['comment'], 'string'],
            [['first_name', 'patronymic', 'last_name', 'telephone', 'country', 'city', 'region', 'date_of_birth', 'work'], 'string', 'max' => 50],
            [['bad_habits'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'patronymic' => 'Patronymic',
            'last_name' => 'Last Name',
            'telephone' => 'Telephone',
            'country' => 'Country',
            'city' => 'City',
            'region' => 'Region',
            'date_of_birth' => 'Date Of Birth',
            'work' => 'Work',
            'comment' => 'Comment',
            'bad_habits' => 'Bad Habits',
        ];
    }
}
