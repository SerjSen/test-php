<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "distance".
 *
 * @property integer $id
 * @property integer $car_distance
 * @property integer $count_distance
 */
class Distance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_distance', 'count_distance'], 'required'],
            [['car_distance', 'count_distance'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_distance' => 'Car Distance',
            'count_distance' => 'Count Distance',
        ];
    }
}
