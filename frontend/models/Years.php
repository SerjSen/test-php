<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "years".
 *
 * @property integer $id
 * @property integer $car_year
 * @property integer $count_years
 */
class Years extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'years';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_year', 'count_years'], 'required'],
            [['car_year', 'count_years'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_year' => 'Car Year',
            'count_years' => 'Count Years',
        ];
    }
}
